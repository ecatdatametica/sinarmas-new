package com.example;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;

@Controller
public class OutsideController {

   

    @RequestMapping("/d/firstdata")
    public @ResponseBody String greeting() {
    	RestTemplate restTemplate = new RestTemplate();
    	 

        String quote = restTemplate.getForObject("http://10.200.99.107:8080/data-service/clickstream/data?dimensions=load_date,bu,province,distrik&metrics=ACT_WOOD_PROD_MTD_CU,TRGT_WOOD_PROD_MTD_CU,ACT_WOOD_PROD_YTD_CU,TRGT_WOOD_PROD_YTD_CU,ACT_PLANT_MTD_CU,TRGT_PLANT_MTD_CU,ACT_PLANT_YTD_CU,TRGT_PLANT_YTD_CU,ACT_NURSERY_MTD_CU,TRGT_NURSARY_MTD_CU,ACT_NURSERY_YTD_CU,TRGT_NURSARY_YTD_CU,ACT_WOOD_PROD_MTD_PR,TRGT_WOOD_PROD_MTD_PR,ACT_WOOD_PROD_YTD_PR,TRGT_WOOD_PROD_YTD_PR,ACT_PLANT_MTD_PR,TRGT_PLANT_MTD_PR,ACT_PLANT_YTD_PR,TRGT_PLANT_YTD_PR,ACT_NURSERY_MTD_PR,TRGT_NURSARY_MTD_PR,ACT_NURSERY_YTD_PR,TRGT_NURSARY_YTD_PR&fromDate=2015-09-29&toDate=2015-09-29", String.class);
        return quote;
    }



     @RequestMapping("/predictive/{date}")
        public @ResponseBody String dateBasedPredictive(@PathVariable String date) {
        	RestTemplate restTemplate = new RestTemplate();

            String url = "http://10.200.99.107:8080/data-service/clickstream/data?dimensions=load_date_3,bu,province,distrik&metrics=ONE_MONTH,THREE_MONTHS,SIX_MONTHS&fromDate="+date+"&toDate="+date;
            String quote = restTemplate.getForObject(url, String.class);
            return quote;
        }

        @RequestMapping("/harvesting/{date}")
                public @ResponseBody String dateBasedHarvesting(@PathVariable String date) {
                	RestTemplate restTemplate = new RestTemplate();

                    String url1 = "http://10.200.99.107:8080/data-service/clickstream/data?dimensions=load_date_1,bu,province,distrik&metrics=PETAK_ID,HARVESTING_COMPLETED,START_DATE,END_DATE,ACT_WOOD_POTENCY,TRGT_WOOD_POTENCY,TOTAL_COST,CONTRACTOR,TYPE_OF_WORK&fromDate="+date+"&toDate="+date;
                    String url = "http://10.200.99.107:8080/data-service/clickstream/data?dimensions=load_date_1,bu,province,distrik&metrics=PETAK_ID,HARVESTING_COMPLETED,START_DATE,END_DATE,ACT_WOOD_POTENCY,TRGT_WOOD_POTENCY,TOTAL_COST,CONTRACTOR,TYPE_OF_WORK,DURATION,TONNAGE,AVG_COST_PER_TONNAGE,WOOD_POTENCY_DIFF&fromDate="+date+"&toDate="+date;
                    String quote = restTemplate.getForObject(url, String.class);
                    return quote;
                }

                @RequestMapping("/plantation/{date}")
                         public @ResponseBody String dateBasedPlantation(@PathVariable String date) {
                         	RestTemplate restTemplate = new RestTemplate();


                             String url1 = "http://10.200.99.107:8080/data-service/clickstream/data?dimensions=load_date_2,bu,province,distrik,PLOT_ID,ROTATION&metrics=PLANTATION_COMPLETED,PLANTATION_START_DATE,PLANTATION_END_DATE,MAINTENANCE_START_DATE,MAINTENANCE_END_DATE,NURSERY_ORIGIN,TOTAL_COST,CONTRACTOR,TYPE_OF_WORK&filters=ROTATION EQ 4&fromDate="+date+"&toDate="+date;
                             String url  = "http://10.200.99.107:8080/data-service/clickstream/data?dimensions=load_date_2,bu,province,distrik,PLOT_ID,ROTATION&metrics=PLANTATION_COMPLETED,PLANTATION_START_DATE,PLANTATION_END_DATE,MAINTENANCE_START_DATE,MAINTENANCE_END_DATE,NURSERY_ORIGIN,TOTAL_COST,CONTRACTOR,TYPE_OF_WORK,AVG_COST_PER_HECTARE,PLANTATION_DATE_DIFF,MAINTENANCE_DATE_DIFF,SPECIES,HECTARE,PLANTATION_DATE,LAND_TYPE&filters=ROTATION EQ 4&fromDate="+date+"&toDate="+date;
                             String quote = restTemplate.getForObject(url, String.class);
                             return quote;
                         }

                            @RequestMapping("/plantationHistory/{date}/{id}")
                                                  public @ResponseBody String dateBasedPlantationHistory(@PathVariable String date,@PathVariable String id) {
                                                  	RestTemplate restTemplate = new RestTemplate();

                                                      String url = "http://10.200.99.107:8080/data-service/clickstream/data?dimensions=load_date_2,bu,province,distrik,PLOT_ID,ROTATION&metrics=PLANTATION_COMPLETED,PLANTATION_START_DATE,PLANTATION_END_DATE,MAINTENANCE_START_DATE,MAINTENANCE_END_DATE,NURSERY_ORIGIN,TOTAL_COST,CONTRACTOR,TYPE_OF_WORK&filters=ROTATION EQ 3,PLOT_ID EQ "+id+"&fromDate="+date+"&toDate="+date;
                                                      String quote = restTemplate.getForObject(url, String.class);
                                                      return quote;
                                                  }

    @RequestMapping("/snddata/{date}")
    public @ResponseBody String dateBased(@PathVariable String date) {
    	RestTemplate restTemplate = new RestTemplate();
    	 
        String url = "http://10.200.99.107:8080/data-service/clickstream/data?dimensions=load_date,bu,province,distrik&metrics=ACT_WOOD_PROD_MTD_CU,TRGT_WOOD_PROD_MTD_CU,ACT_WOOD_PROD_YTD_CU,TRGT_WOOD_PROD_YTD_CU,ACT_PLANT_MTD_CU,TRGT_PLANT_MTD_CU,ACT_PLANT_YTD_CU,TRGT_PLANT_YTD_CU,ACT_NURSERY_MTD_CU,TRGT_NURSARY_MTD_CU,ACT_NURSERY_YTD_CU,TRGT_NURSARY_YTD_CU,ACT_WOOD_PROD_MTD_PR,TRGT_WOOD_PROD_MTD_PR,ACT_WOOD_PROD_YTD_PR,TRGT_WOOD_PROD_YTD_PR,ACT_PLANT_MTD_PR,TRGT_PLANT_MTD_PR,ACT_PLANT_YTD_PR,TRGT_PLANT_YTD_PR,ACT_NURSERY_MTD_PR,TRGT_NURSARY_MTD_PR,ACT_NURSERY_YTD_PR,TRGT_NURSARY_YTD_PR&fromDate="+date+"&toDate="+date;
        String quote = restTemplate.getForObject(url, String.class);
        return quote;
    }

}