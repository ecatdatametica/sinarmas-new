'use strict';

angular.module('demoAppApp')
    .controller('kpi2Controller', function ($scope, Principal,$mdDialog,DataSource,$rootScope) {
        Principal.identity().then(function(account) {
            $scope.account = account;
            $scope.isAuthenticated = Principal.isAuthenticated;
            $scope.harvestingProvince = [
                "All",
                "Jambi"

            ];

            $scope.harvestingPetak = [];


            $scope.harvestingDistrik = [
                "All",
                "DA"

            ];
            $scope.harvestingRegion = [
                {
                    name: 'All'
                },

                {
                name: 'Jambi'
                    }


            ];
            $scope.Province='All'
            $scope.Region='All'
            $scope.Distrik='All'
            $scope.Petaktotalcost=[]
            $scope.avgwoodpotencylist=[]
            $scope.trgwoodpotencylist=[]
            $rootScope.Petaked='All'


            $scope.harvestingPetak.push('All')
            $scope.rendFunctHarvesting=function(dataSelect,dataid){
                $scope.totalnumber=0;
                $scope.avgwoodpotency=0;
                $scope.avgwood=0;
                $scope.trgwoodpotency=0;
                $scope.trgwood=0;
                $scope.MTDdataMaster=[]
                $scope.valueone=''
                $scope.columnHeaders=[]
                $scope.datachartGraph=[]
                $scope.duration=0;
                $scope.total_tonnage=0;

                DataSource.dashboardHarvesting(dataSelect).then(function (data) {
               /*     angular.copy(data,$scope.MTDdataMaster)
                    angular.forEach($scope.MTDdataMaster.rows[0],function(thisdatalist,key) {
                        $scope.MTDdataMaster.columnHeaders[key].valueone=thisdatalist
                    });*/


                    if(dataid=="All"){
                        $scope.Province='All'
                        $scope.Region='All'
                        $scope.Distrik='All'
                        angular.forEach(data.rows,function(thisdatalist,key) {
                            console.log(thisdatalist[4])
                            $scope.harvestingPetak.push(thisdatalist[4])
                            $scope.Petaktotalcost.push(thisdatalist[10])
                            $scope.avgwoodpotencylist.push(thisdatalist[8])
                            $scope.trgwoodpotencylist.push(thisdatalist[9])

                            $scope.total_tonnage = parseFloat(thisdatalist[14]).toFixed(2);
                            $scope.avg_tonnage = parseFloat(thisdatalist[15]).toFixed(2);



                        });
                        var totalnumbe=0
                        for(var i in $scope.Petaktotalcost) {
                            totalnumbe += parseInt($scope.Petaktotalcost[i]);

                        }
                        $scope.totalnumber=totalnumbe.toLocaleString()
                        for(var i in $scope.avgwoodpotencylist) {

                            var avgwoodnum =parseFloat($scope.avgwoodpotencylist[i]).toFixed(2);

                            $scope.avgwood +=parseFloat(avgwoodnum)
                            $scope.avgwoodpotency=($scope.avgwood/5).toFixed(2)

                        }
                        for(var i in $scope.trgwoodpotencylist) {

                            var trgnum =parseFloat($scope.trgwoodpotencylist[i]).toFixed(2);

                            $scope.trgwood +=parseFloat(trgnum)
                            $scope.trgwoodpotency=($scope.trgwood/5).toFixed(2)

                        }
                        $scope.diffsecond =   parseFloat($scope.avgwoodpotency) - parseFloat($scope.trgwoodpotency)

                    }else{
                        $scope.Province='Jambi'
                        $scope.Region='Jambi'
                        $scope.Distrik='DA'
                        angular.forEach(data.rows,function(thisdatalist,key) {

                           if(thisdatalist[4]==dataid){
                               $scope.diffsecond = thisdatalist[16];
                               console.log(thisdatalist)
                               $scope.avgwoodpotency=thisdatalist[8]
                               $scope.trgwoodpotency=thisdatalist[9]
                               $scope.totalnumber=Math.round(thisdatalist[10]).toLocaleString()
                               $scope.totalnumber=Math.round(thisdatalist[10]).toLocaleString()
                               $scope.startDateHarvesting=thisdatalist[6]
                               $scope.EndDateHarvesting=thisdatalist[7]
                               $scope.startDatePlantation=thisdatalist[7]
                               $scope.comHarvesting=thisdatalist[5]


                               /*$scope.duration =Math.round(thisdatalist[13]).toLocaleString();
                               $scope.total_tonnage = Math.round(thisdatalist[14]).toLocaleString();
                               $scope.avg_tonnage = Math.round(thisdatalist[15]).toLocaleString();
                               */
                               $scope.duration =parseFloat(thisdatalist[13]).toFixed(2);
                               $scope.total_tonnage = parseFloat(thisdatalist[14]).toFixed(2);
                               $scope.avg_tonnage = parseFloat(thisdatalist[15]).toFixed(2);

                               console.log($scope.duration)
                               $scope.datachartGraph=[
                                   { "name" : thisdatalist[4], "parent":"null" },
                                   { "name" : thisdatalist[11], "parent":thisdatalist[4] }
                               ]
                               var re = /\s*;\s*/;
                               var nameList = thisdatalist[12].split(re);


                               angular.forEach(nameList,function(thisdatal,key) {
                                   $scope.datachartGraph.push({
                                       'name':thisdatal,
                                       'parent':thisdatalist[11]
                                   })

                               });

                               $scope.chartrender($scope.datachartGraph)
                           }

                        });

                    }

                    /*$scope.diffsecond =   parseFloat($scope.trgwoodpotency) - parseFloat($scope.avgwoodpotency);
*/

                   



                });

            }

            // $scope.user.submissionDate=''
            var dataSelect='2015-10-02'
            $scope.dataid='All'
            $scope.renderkpi1Chart=function(data){

               /* if( typeof $scope.user !='undefined'){
                    var dataSelect=$filter('date')($scope.user.submissionDate, "yyyy-MM-dd");
                    $scope.rendFunctPredictive(dataSelect)
                }*/
                console.log(data)
                $scope.rendFunctHarvesting(dataSelect,data)
            }

            $scope.rendFunctHarvesting(dataSelect,$scope.dataid)


            $scope.changekpifuntion=function(data){
                DataSource.setpiedata(data)
            }


















            var chartPetak = c3.generate({
                bindto: '#chartPetak',
                data: {
                    columns: [
                        ['Completed Petaks', 3]
                    ],
                    type: 'gauge'
                },
                gauge: {
                    min: 0,
                    max: 5
                }
            });
            $scope.startvalue=40
            $scope.sendvalue=80
    $scope.startdate=new Date()


            $(function() {
                $('input[name="daterange"]').daterangepicker();
            });



    /*        var data = [
                { "name" : "Level A", "parent":"Top Level" },
                { "name" : "Top Level", "parent":"null" },
                { "name" : "Son of A", "parent":"Level A" },
                { "name" : "Daughter of A", "parent":"Level A" },
                { "name" : "Level 2: B", "parent":"Top Level" }
            ];*/
           /* chart code start*/
            $scope.chartrender=function(data){

                // *********** Convert flat data into a nice tree ***************
                // create a name: node map

                var dataMap = data.reduce(function(map, node) {
                    map[node.name] = node;
                    return map;
                }, {});
                // create the tree array
                var treeData = [];
                data.forEach(function(node) {
                    // add to parent
                    var parent = dataMap[node.parent];
                    if (parent) {
                        // create child array if it doesn't exist
                        (parent.children || (parent.children = []))
                            // add node to child array
                            .push(node);
                    } else {
                        // parent is null or missing
                        treeData.push(node);
                    }
                });
                // ************** Generate the tree diagram  *****************
                var closeNodeFillColor = "steelblue";

                var margin = {top: 10, right: 120, bottom: 10, left: 120},
                    width = 960 - margin.right - margin.left,
                    height = 300 - margin.top - margin.bottom;

                var i = 0,
                    duration = 750,
                    root;

                var tree = d3.layout.tree()
                    .size([height, width]);

                var diagonal = d3.svg.diagonal()
                    .projection(function(d) { return [d.y, d.x]; });
                d3.select("body #chartPetakrender svg").remove()
                var svg = d3.select("body #chartPetakrender").append("svg")
                    .attr("width", width + margin.right + margin.left)
                    .attr("height", height + margin.top + margin.bottom)
                    .append("g")
                    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

                root = treeData[0];
                root.x0 = height / 2;
                root.y0 = 0;

                update(root);

                d3.select(self.frameElement).style("height", "300px");

                function update(source) {

                    // Compute the new tree layout.
                    var nodes = tree.nodes(root).reverse(),
                        links = tree.links(nodes);

                    // Normalize for fixed-depth.
                    nodes.forEach(function(d) { d.y = d.depth * 180; });

                    // Update the nodes…
                    var node = svg.selectAll("g.node")
                        .data(nodes, function(d) { return d.id || (d.id = ++i); });

                    // Enter any new nodes at the parent's previous position.
                    var nodeEnter = node.enter().append("g")
                        .attr("class", "node")
                        .attr("transform", function(d) { return "translate(" + source.y0 + "," + source.x0 + ")"; })
                        .on("click", click);

                    nodeEnter.append("circle")
                        .attr("r", 1e-6)
                        .style("fill", function(d) { return d._children ? "lightsteelblue" : "#fff"; });

                    nodeEnter.append("text")
                        .attr("x", function(d) { return d.children || d._children ? -13 : 13; })
                        .attr("dy", ".35em")
                        .attr("text-anchor", function(d) { return d.children || d._children ? "end" : "start"; })
                        .text(function(d) { return d.name; })
                        .style("fill-opacity", 1e-6);

                    // Transition nodes to their new position.
                    var nodeUpdate = node.transition()
                        .duration(duration)
                        .attr("transform", function(d) { return "translate(" + d.y + "," + d.x + ")"; });

                    nodeUpdate.select("circle")
                        .attr("r", 10)
                        .style("fill", function(d) { return d._children ? closeNodeFillColor : "#fff"; });

                    nodeUpdate.select("text")
                        .style("fill-opacity", 1);

                    // Transition exiting nodes to the parent's new position.
                    var nodeExit = node.exit().transition()
                        .duration(duration)
                        .attr("transform", function(d) { return "translate(" + source.y + "," + source.x + ")"; })
                        .remove();

                    nodeExit.select("circle")
                        .attr("r", 1e-6);

                    nodeExit.select("text")
                        .style("fill-opacity", 1e-6);

                    // Update the links…
                    var link = svg.selectAll("path.link")
                        .data(links, function(d) { return d.target.id; });

                    // Enter any new links at the parent's previous position.
                    link.enter().insert("path", "g")
                        .attr("class", "link")
                        .attr("d", function(d) {
                            var o = {x: source.x0, y: source.y0};
                            return diagonal({source: o, target: o});
                        });

                    // Transition links to their new position.
                    link.transition()
                        .duration(duration)
                        .attr("d", diagonal);

                    // Transition exiting nodes to the parent's new position.
                    link.exit().transition()
                        .duration(duration)
                        .attr("d", function(d) {
                            var o = {x: source.x, y: source.y};
                            return diagonal({source: o, target: o});
                        })
                        .remove();

                    // Stash the old positions for transition.
                    nodes.forEach(function(d) {
                        d.x0 = d.x;
                        d.y0 = d.y;
                    });
                }


// Toggle children on click.
                function click(d) {
                    if (d.children) {
                        d._children = d.children;
                        d.children = null;
                    } else {
                        d.children = d._children;
                        d._children = null;
                    }
                    update(d);
                }

            }


            /*chart code end*/


            $scope.showAdvanced = function(ev) {
                $mdDialog.show({
                    controller: popcontrollerlist,
                    templateUrl: '../../scripts/app/main/contractorwork.tmpl.html',
                    parent: angular.element(document.body),
                    targetEvent: ev,
                    clickOutsideToClose:true

                })
                    .then(function(answer) {
                        $scope.status = 'You said the information was "' + answer + '".';
                    }, function() {
                        $scope.status = 'You cancelled the dialog.';
                    });
            };


            var height = 100;
            var width  = 100;

            var vis = d3.select("body #chartpolygons1").append("svg")
                .attr("width", width).attr("height", height)

            d3.json("scripts/app/main/single_geojson/polygon1.json", function(json) {

                var projection = d3.geo.projection(function(x, y) { return [x, y];})
                    .precision(0).scale(1).translate([0, 0]);

                var path = d3.geo.path().projection(projection);

                var bounds = path.bounds(json),
                    scale  = .95 / Math.max((bounds[1][0] - bounds[0][0]) / width,
                            (bounds[1][1] - bounds[0][1]) / height),
                    transl = [(width - scale * (bounds[1][0] + bounds[0][0])) / 2,
                        (height - scale * (bounds[1][1] + bounds[0][1])) / 2];

                projection.scale(scale).translate(transl);

                vis.selectAll("path").data(json.features).enter().append("path")
                    .attr("d", path)
                    .style("fill", "rgb(31, 119, 180)")
                    .style("stroke-width", "1px")
                    .style("stroke", "#ccc")
            });
            var vis1 = d3.select("body #chartpolygons2").append("svg")
                .attr("width", width).attr("height", height)

            d3.json("scripts/app/main/single_geojson/polygon2.json", function(json) {

                var projection = d3.geo.projection(function(x, y) { return [x, y];})
                    .precision(0).scale(1).translate([0, 0]);

                var path = d3.geo.path().projection(projection);

                var bounds = path.bounds(json),
                    scale  = .95 / Math.max((bounds[1][0] - bounds[0][0]) / width,
                            (bounds[1][1] - bounds[0][1]) / height),
                    transl = [(width - scale * (bounds[1][0] + bounds[0][0])) / 2,
                        (height - scale * (bounds[1][1] + bounds[0][1])) / 2];

                projection.scale(scale).translate(transl);

                vis1.selectAll("path").data(json.features).enter().append("path")
                    .attr("d", path)
                    .style("fill", "rgb(31, 119, 180)")
                    .style("stroke-width", "1px")
                    .style("stroke", "#ccc")
            });

            var vis2 = d3.select("body #chartpolygons3").append("svg")
                .attr("width", width).attr("height", height)

            d3.json("scripts/app/main/single_geojson/polygon3.json", function(json) {

                var projection = d3.geo.projection(function(x, y) { return [x, y];})
                    .precision(0).scale(1).translate([0, 0]);

                var path = d3.geo.path().projection(projection);

                var bounds = path.bounds(json),
                    scale  = .95 / Math.max((bounds[1][0] - bounds[0][0]) / width,
                            (bounds[1][1] - bounds[0][1]) / height),
                    transl = [(width - scale * (bounds[1][0] + bounds[0][0])) / 2,
                        (height - scale * (bounds[1][1] + bounds[0][1])) / 2];

                projection.scale(scale).translate(transl);

                vis2.selectAll("path").data(json.features).enter().append("path")
                    .attr("d", path)
                    .style("fill", "rgb(31, 119, 180)")
                    .style("stroke-width", "1px")
                    .style("stroke", "#ccc")
            });



        });
    });
function popcontrollerlist($scope, $mdDialog) {








    $scope.hide = function() {
        $mdDialog.hide();
    };
    $scope.cancel = function() {
        $mdDialog.cancel();
    };
    $scope.answer = function(answer) {
        $mdDialog.hide(answer);
    };
}
