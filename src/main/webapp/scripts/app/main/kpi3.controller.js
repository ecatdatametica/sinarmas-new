'use strict';

angular.module('demoAppApp')
    .controller('kpi3Controller', function ($scope, Principal,$mdDialog,DataSource,$rootScope) {
        Principal.identity().then(function(account) {
            $scope.account = account;
            $scope.isAuthenticated = Principal.isAuthenticated;
            $scope.harvestingProvince = [
                "All",
                "Jambi"

            ];
            $scope.harvestingPetak = [];
            $scope.ss=0;


            $scope.harvestingDistrik = [
                "All",
                "DA"

            ];
            $scope.harvestingRegion = [
                {  name: 'Jambi' },
                {  name: 'All' }

            ];
            $scope.Province='All'
            $scope.Region='All'
            $scope.Distrik='All'
            $scope.Petaktotalcost=[]
            $scope.avgwoodpotencylist=[]
            $scope.trgwoodpotencylist=[]
           //$scope.Petak='All'


            $scope.harvestingPetak.push('All')
            $scope.rendFunctHarvesting=function(dataSelect,dataid){

                $scope.totalnumber=0;
                $scope.avgwoodpotency=0;
                $scope.avgwood=0;
                $scope.trgwoodpotency=0;
                $scope.trgwood=0;
                $scope.MTDdataMaster=[]
                $scope.valueone=''
                $scope.columnHeaders=[]
                $scope.datachartGraph=[]

                $scope.plantationDate='x';
                $scope.LandType='y';
                $scope.species='z';
                $scope.hectorage='q';
                $scope.total_hectorage=0;
                $scope.avg_hectorage=0;

                DataSource.dashboardPlantationlist(dataSelect).then(function (data) {
                    console.log(data)
                    /*     angular.copy(data,$scope.MTDdataMaster)
                     angular.forEach($scope.MTDdataMaster.rows[0],function(thisdatalist,key) {
                     $scope.MTDdataMaster.columnHeaders[key].valueone=thisdatalist
                     });*/
                    start1();
                    function start1(){
                    var rp1 = radialProgress(document.getElementById('div12'))

                        .diameter(80)
                        .value(10)
                        .render();
                    }

                    if(dataid=="All"){
                        $scope.Province='All'
                        $scope.Region='All'
                        $scope.Distrik='All'
                        angular.forEach(data.rows,function(thisdatalist,key) {
                            console.log(thisdatalist[4])
                            $scope.harvestingPetak.push(thisdatalist[4])
                            $scope.Petaktotalcost.push(thisdatalist[12])
                            $scope.avgwoodpotencylist.push(thisdatalist[8])
                            $scope.trgwoodpotencylist.push(thisdatalist[9])
                            $scope.ss=thisdatalist[18];

                            $scope.total_hectorage=parseFloat(thisdatalist[19]).toFixed(2);
                            $scope.avg_hectorage=Math.round(thisdatalist[15]).toLocaleString()

                            $scope.plantationDate=thisdatalist[20];
                            $scope.LandType=thisdatalist[21];
                            $scope.species=thisdatalist[18];
                            $scope.hectorage=parseFloat(thisdatalist[19]).toFixed(2);



                        });
                        var totalnumbe=0
                        for(var i in $scope.Petaktotalcost) {
                            totalnumbe += parseInt($scope.Petaktotalcost[i]);
                        }
                        $scope.totalnumber=totalnumbe.toLocaleString()
                        for(var i in $scope.avgwoodpotencylist) {

                            var avgwoodnum =parseFloat($scope.avgwoodpotencylist[i]).toFixed(2);

                            $scope.avgwood +=parseFloat(avgwoodnum)
                            $scope.avgwoodpotency=($scope.avgwood/5).toFixed(2)

                        }
                        for(var i in $scope.trgwoodpotencylist) {

                            var trgnum =parseFloat($scope.trgwoodpotencylist[i]).toFixed(2);

                            $scope.trgwood +=parseFloat(trgnum)
                            $scope.trgwoodpotency=($scope.trgwood/5).toFixed(2)

                        }


                    }else{
                        $scope.Province='Jambi'
                        $scope.Region='Jambi'
                        $scope.Distrik='DA'
                        angular.forEach(data.rows,function(thisdatalist,key) {

                            if(thisdatalist[4]==dataid){
                                console.log(thisdatalist)
                                $scope.avgwoodpotency=thisdatalist[8]
                                $scope.trgwoodpotency=thisdatalist[9]
                                $scope.totalnumber=Math.round(thisdatalist[12]).toLocaleString()
                                $scope.startDateHarvesting=thisdatalist[9]
                                $scope.EndDateHarvesting=thisdatalist[10]
                                $scope.EndDatePlantation=thisdatalist[8]
                                $scope.startDatePlantation=thisdatalist[7]
                                $scope.comHarvesting=thisdatalist[6]

                                $scope.total_hectorage=parseFloat(thisdatalist[19]).toFixed(2);
                                $scope.avg_hectorage=Math.round(thisdatalist[15]).toLocaleString()

                                $scope.plantationDate=thisdatalist[20];
                                $scope.LandType=thisdatalist[21];
                                $scope.species=thisdatalist[18];
                                $scope.hectorage=parseFloat(thisdatalist[19]).toFixed(2);

                                $scope.plantationDateDiff=parseFloat(thisdatalist[16]).toFixed(2);
                                $scope.maintainceDateDiff=parseFloat(thisdatalist[17]).toFixed(2);


                                var re = /\s*;\s*/;
                                var nameList = thisdatalist[13].split(re);
                                var nameListone = thisdatalist[11].split(re);

                                console.log(nameList)


                                var nameListnew = thisdatalist[11].split(re);
                                var str =thisdatalist[14].replace(/;/g , ',');
                                var jsonObj = $.parseJSON('[' + str + ']');
                                angular.copy(jsonObj,$scope.datachartGraph)
                                $scope.datachartGraph.push({ "name" : thisdatalist[4], "parent":"null" })

                                angular.forEach(nameList,function(thisdatal,key) {
                                    $scope.datachartGraph.push({
                                        'name':thisdatal,
                                        'parent':thisdatalist[4]
                                    })

                                });


                            //    $scope.datachartGraph.push(jsonObj)
                                console.log($scope.datachartGraph)
                               $scope.functr(nameListone)
                             $scope.chartrender($scope.datachartGraph)
                            }

                        });


                    }



                });

            }



            // $scope.user.submissionDate=''
            var dataSelect='2015-11-02'
            $scope.dataid='All'
            $scope.renderkpi1Chart=function(data){

                /* if( typeof $scope.user !='undefined'){
                 var dataSelect=$filter('date')($scope.user.submissionDate, "yyyy-MM-dd");
                 $scope.rendFunctPredictive(dataSelect)
                 }*/
                console.log(data)
                $scope.rendFunctHarvesting(dataSelect,data)
            }

            var item=DataSource.getpiedata()
            console.log(item)
            if(item==undefined){


            }else{
                $scope.Petaker=item
                var dataone=dataSelect+'/'+item

            }

            $scope.Petaker='All'
            $scope.rendFunctHarvesting(dataSelect,$scope.dataid)













            var chartPetak = c3.generate({
                bindto: '#chartPetak',
                data: {
                    columns: [
                        ['Completed Petaks', 3]
                    ],
                    type: 'gauge'
                },
                gauge: {
                    min: 0,
                    max: 5
                }
            });
            $scope.startvalue=40
            $scope.sendvalue=80
            $scope.startdate=new Date()


            $(function() {
                $('input[name="daterange"]').daterangepicker();
            });

            $scope.functr=function(data){







                // Fake JSON data


                // D3 Bubble Chart

                var diameter = 200;
                d3.select("body #chartmain svg").remove()
                var svg = d3.select('body #chartmain').append('svg')
                    .attr('width', diameter)
                    .attr('height', diameter);

                var bubble = d3.layout.pack()
                    .size([diameter, diameter])
                    .value(function(d) {return d.size;})
                    // .sort(function(a, b) {
                    // 	return -(a.value - b.value)
                    // })
                    .padding(6);

                // generate data with calculated layout values
                var nodes = bubble.nodes(processData(data))
                    .filter(function(d) { return !d.children; }); // filter out the outer bubble

             /*   var vis = svg.selectAll('svg')
                    .data(nodes);

                vis.enter().append('circle')
                    .attr('transform', function(d) { return 'translate(' + d.x + ',' + d.y + ')'; })
                    .attr('r', function(d) { return d.r; })
                    .attr('fill', 'red')
                    .attr('class', 'colorR');

                vis.enter().append("text")
                    .attr("dy", ".3em")
                    .attr('fill', 'red')
                    .attr('class', 'colorRtext')
                    .style("text-anchor", "middle")
                    .text(function(d) { return d.className.substring(0, d.r / 3); });*/

                var color = d3.scale.category20();
                var node = svg.selectAll("svg")
                    .data(nodes)

                    .enter().append("g")
                    .attr("class", "node44")
                    .attr("transform", function(d) { return "translate(" + d.x + "," + d.y + ")"; });

                node.append("title")
                    .text("sfas");

                node.append("circle")
                    .attr('r', function(d) { return d.r; })
                    .attr("fill",function(d,i){return color(i);});

                node.append("text")
                    .attr("dy", ".3em")
                    .style("text-anchor", "middle")
                    .text(function(d) {
                        return d.name;
                    });

                function processData(data) {
                    var obj = data;

                    var newDataSet = [];
                    for (var i = 0; i < obj.length; i++) {
                   //     alert(myStringArray[i]);
                        newDataSet.push({name: obj[i], className: obj[i].toLowerCase(), size: 100});
                        //Do something
                    }
              /*      for(var prop in obj) {
                        console.log(prop)

                    }*/
                    return {children: newDataSet};
                }














            }


            /* chart code start*/
            $scope.chartrender=function(data){

                // *********** Convert flat data into a nice tree ***************
                // create a name: node map

                var dataMap = data.reduce(function(map, node) {
                    map[node.name] = node;
                    return map;
                }, {});
                // create the tree array
                var treeData = [];
                data.forEach(function(node) {
                    // add to parent
                    var parent = dataMap[node.parent];
                    if (parent) {
                        // create child array if it doesn't exist
                        (parent.children || (parent.children = []))
                            // add node to child array
                            .push(node);
                    } else {
                        // parent is null or missing
                        treeData.push(node);
                    }
                });
                // ************** Generate the tree diagram  *****************
                var closeNodeFillColor = "steelblue";

                var margin = {top: 10, right: 120, bottom: 10, left: 120},
                    width = 960 - margin.right - margin.left,
                    height = 300 - margin.top - margin.bottom;

                var i = 0,
                    duration = 750,
                    root;

                var tree = d3.layout.tree()
                    .size([height, width]);

                var diagonal = d3.svg.diagonal()
                    .projection(function(d) { return [d.y, d.x]; });
                d3.select("body #chartPetakrender svg").remove()
                var svg = d3.select("body #chartPetakrender").append("svg")
                    .attr("width", width + margin.right + margin.left)
                    .attr("height", height + margin.top + margin.bottom)
                    .append("g")
                    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

                root = treeData[0];
                root.x0 = height / 2;
                root.y0 = 0;

                update(root);

                d3.select(self.frameElement).style("height", "300px");

                function update(source) {

                    // Compute the new tree layout.
                    var nodes = tree.nodes(root).reverse(),
                        links = tree.links(nodes);

                    // Normalize for fixed-depth.
                    nodes.forEach(function(d) { d.y = d.depth * 180; });

                    // Update the nodes…
                    var node = svg.selectAll("g.node")
                        .data(nodes, function(d) { return d.id || (d.id = ++i); });

                    // Enter any new nodes at the parent's previous position.
                    var nodeEnter = node.enter().append("g")
                        .attr("class", "node")
                        .attr("transform", function(d) { return "translate(" + source.y0 + "," + source.x0 + ")"; })
                        .on("click", click);

                    nodeEnter.append("circle")
                        .attr("r", 1e-6)
                        .style("fill", function(d) { return d._children ? "lightsteelblue" : "#fff"; });

                    nodeEnter.append("text")
                        .attr("x", function(d) { return d.children || d._children ? -13 : 13; })
                        .attr("dy", ".35em")
                        .attr("text-anchor", function(d) { return d.children || d._children ? "end" : "start"; })
                        .text(function(d) { return d.name; })
                        .style("fill-opacity", 1e-6);

                    // Transition nodes to their new position.
                    var nodeUpdate = node.transition()
                        .duration(duration)
                        .attr("transform", function(d) { return "translate(" + d.y + "," + d.x + ")"; });

                    nodeUpdate.select("circle")
                        .attr("r", 10)
                        .style("fill", function(d) { return d._children ? closeNodeFillColor : "#fff"; });

                    nodeUpdate.select("text")
                        .style("fill-opacity", 1);

                    // Transition exiting nodes to the parent's new position.
                    var nodeExit = node.exit().transition()
                        .duration(duration)
                        .attr("transform", function(d) { return "translate(" + source.y + "," + source.x + ")"; })
                        .remove();

                    nodeExit.select("circle")
                        .attr("r", 1e-6);

                    nodeExit.select("text")
                        .style("fill-opacity", 1e-6);

                    // Update the links…
                    var link = svg.selectAll("path.link")
                        .data(links, function(d) { return d.target.id; });

                    // Enter any new links at the parent's previous position.
                    link.enter().insert("path", "g")
                        .attr("class", "link")
                        .attr("d", function(d) {
                            var o = {x: source.x0, y: source.y0};
                            return diagonal({source: o, target: o});
                        });

                    // Transition links to their new position.
                    link.transition()
                        .duration(duration)
                        .attr("d", diagonal);

                    // Transition exiting nodes to the parent's new position.
                    link.exit().transition()
                        .duration(duration)
                        .attr("d", function(d) {
                            var o = {x: source.x, y: source.y};
                            return diagonal({source: o, target: o});
                        })
                        .remove();

                    // Stash the old positions for transition.
                    nodes.forEach(function(d) {
                        d.x0 = d.x;
                        d.y0 = d.y;
                    });
                }


// Toggle children on click.
                function click(d) {
                    if (d.children) {
                        d._children = d.children;
                        d.children = null;
                    } else {
                        d.children = d._children;
                        d._children = null;
                    }
                    update(d);
                }

            }

         //   $scope.chartrender()
            /*chart code end*/


            $scope.showAdvancedother = function(ev) {
                $mdDialog.show({
                    controller: popcontrollerlistother,
                    templateUrl: '../../scripts/app/main/contractorwork.tmpl.html',
                    parent: angular.element(document.body),
                    targetEvent: ev,
                    clickOutsideToClose:true

                })
                    .then(function(answer) {
                        $scope.status = 'You said the information was "' + answer + '".';
                    }, function() {
                        $scope.status = 'You cancelled the dialog.';
                    });
            };




            var height = 100;
            var width  = 100;

            var vis = d3.select("body #chartpolygons1").append("svg")
                .attr("width", width).attr("height", height)

            d3.json("../../scripts/app/main/single_geojson/polygon1.json", function(json) {

                var projection = d3.geo.projection(function(x, y) { return [x, y];})
                    .precision(0).scale(1).translate([0, 0]);

                var path = d3.geo.path().projection(projection);

                var bounds = path.bounds(json),
                    scale  = .95 / Math.max((bounds[1][0] - bounds[0][0]) / width,
                            (bounds[1][1] - bounds[0][1]) / height),
                    transl = [(width - scale * (bounds[1][0] + bounds[0][0])) / 2,
                        (height - scale * (bounds[1][1] + bounds[0][1])) / 2];

                projection.scale(scale).translate(transl);

                vis.selectAll("path").data(json.features).enter().append("path")
                    .attr("d", path)
                    .style("fill", "rgb(31, 119, 180)")
                    .style("stroke-width", "1px")
                    .style("stroke", "#ccc")
            });

            var vis2 = d3.select("body #chartpolygons3").append("svg")
                .attr("width", width).attr("height", height)

            d3.json("../../scripts/app/main/single_geojson/polygon3.json", function(json) {

                var projection = d3.geo.projection(function(x, y) { return [x, y];})
                    .precision(0).scale(1).translate([0, 0]);

                var path = d3.geo.path().projection(projection);

                var bounds = path.bounds(json),
                    scale  = .95 / Math.max((bounds[1][0] - bounds[0][0]) / width,
                            (bounds[1][1] - bounds[0][1]) / height),
                    transl = [(width - scale * (bounds[1][0] + bounds[0][0])) / 2,
                        (height - scale * (bounds[1][1] + bounds[0][1])) / 2];

                projection.scale(scale).translate(transl);

                vis2.selectAll("path").data(json.features).enter().append("path")
                    .attr("d", path)
                    .style("fill", "rgb(31, 119, 180)")
                    .style("stroke-width", "1px")
                    .style("stroke", "#ccc")
            });



        });
    });
function popcontrollerlistother($scope, $mdDialog) {
    $scope.hide = function() {
        $mdDialog.hide();
    };
    $scope.cancel = function() {
        $mdDialog.cancel();
    };
    $scope.answer = function(answer) {
        $mdDialog.hide(answer);
    };
}
