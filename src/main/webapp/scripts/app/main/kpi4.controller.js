'use strict';

angular.module('demoAppApp')
    .controller('kpi4Controller', function ($scope, Principal,DataSource) {
        Principal.identity().then(function(account) {
            $scope.account = account;
            $scope.isAuthenticated = Principal.isAuthenticated;

            $scope.sizes = [
                "Jambi"

            ];
            $scope.size='Jambi'
            $scope.name='Jambi'
            $scope.cityname='DA'
            $scope.sizes1 = [
                "MTD",
                "YTD"

            ];

            $scope.city = [
                "DA"


            ];
            $scope.state = [
                {  name: 'Jambi' }

            ];

            $scope.rendFunctPredictive=function(dataSelect){

                $scope.MTDdataMaster=[]
                $scope.valueone=''
                $scope.columnHeaders=[]
                console.log(dataSelect)
                DataSource.dashboardPlantation(dataSelect).then(function (data) {
                    console.log(data)
                    angular.copy(data,$scope.MTDdataMaster)
                    angular.forEach($scope.MTDdataMaster.rows[0],function(thisdatalist,key) {
                        $scope.MTDdataMaster.columnHeaders[key].valueone=thisdatalist
                    });
                   /* var actplantationOneData = $filter('filter')($scope.MTDdataMaster.columnHeaders, { name: "ACT_PLANT_MTD_CU" });
                    var trgplantationOneData = $filter('filter')($scope.MTDdataMaster.columnHeaders, { name: "TRGT_PLANT_MTD_CU" });
                    var actplantationPR = $filter('filter')($scope.MTDdataMaster.columnHeaders, { name: "ACT_PLANT_MTD_PR" });
                    var trgplantationPR = $filter('filter')($scope.MTDdataMaster.columnHeaders, { name: "TRGT_PLANT_MTD_PR" });

                    var datacurrentdata=(actplantationOneData[0].valueone/trgplantationOneData[0].valueone)*100
                    var datapreviewsdata=(actplantationPR[0].valueone/trgplantationPR[0].valueone)*100


                    var actNurseryOneData = $filter('filter')($scope.MTDdataMaster.columnHeaders, { name: "ACT_NURSERY_MTD_CU" });
                    var trgNurseryOneData = $filter('filter')($scope.MTDdataMaster.columnHeaders, { name: "TRGT_NURSARY_MTD_CU" });
                    var actNurseryPR = $filter('filter')($scope.MTDdataMaster.columnHeaders, { name: "ACT_NURSERY_MTD_PR" });
                    var trgNurseryPR = $filter('filter')($scope.MTDdataMaster.columnHeaders, { name: "TRGT_NURSARY_MTD_PR" });*/

                    console.log($scope.MTDdataMaster.columnHeaders[4].valueone,$scope.MTDdataMaster.columnHeaders[5].valueone,$scope.MTDdataMaster.columnHeaders[6].valueone)
                    var chartmain = c3.generate({
                        bindto: '#chartmix',
                        data: {
                            x : 'x',
                            columns: [
                                ['x', '1 Month', '3 Months', '6 Months'],
                                ['Wood Production', $scope.MTDdataMaster.columnHeaders[4].valueone,$scope.MTDdataMaster.columnHeaders[5].valueone,$scope.MTDdataMaster.columnHeaders[6].valueone]

                            ],
                            groups: [
                                ['Wood Production']
                            ],
                            type: 'bar'

                        },
                            bar: {
                                width: {ratio: 0.1}
                            },
                        axis: {

                            x: {
                                type: 'category' // this needed to load string x value
                            }
                        }


                    });

                });

            }

            // $scope.user.submissionDate=''
            $scope.renderkpi1Chart=function(){

                if( typeof $scope.user !='undefined'){
                    var dataSelect=$filter('date')($scope.user.submissionDate, "yyyy-MM-dd");
                    $scope.rendFunctPredictive(dataSelect)
                }

            }
            var dataSelect='2015-11-02'
//hello
            $scope.rendFunctPredictive(dataSelect)































            var chartmain1 = c3.generate({
                bindto: '#chartmain1',
                data: {
                    columns: [
                        ['data', 0.0]
                    ],
                    type: 'gauge'
                },
                gauge: {
                    min: -100,
                    max: 100
                }
            });









        });
    });
