'use strict';

angular.module('demoAppApp')
    .config(function ($stateProvider) {

        $stateProvider
            .state('home', {
                parent: 'site',
                url: '/kpi1',
                data: {
                    authorities: []
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/main/main.html',
                        controller: 'MainController'

                    }
                },
                resolve: {

                }
            });
        $stateProvider
            .state('kpi2', {

                parent: 'site',
                url: '/kpi2',
                data: {
                    authorities: []
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/main/kpi2.html',
                        controller: 'kpi2Controller'
                    }
                },
                resolve: {

                }
            });
        $stateProvider
            .state('kpi3', {
                parent: 'site',
                url: '/kpi3',
                data: {
                    authorities: []
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/main/kpi3.html',
                        controller: 'kpi3Controller'
                    }
                },
                resolve: {

                }
            });
        $stateProvider
            .state('kpi31', {
                parent: 'site',
                url: '/kpi31',
                data: {
                    authorities: []
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/main/kpi31.html',
                        controller: 'kpi31Controller'
                    }
                },
                resolve: {

                }
            });
        $stateProvider
            .state('kpi4', {
                parent: 'site',
                url: '/kpi4',
                data: {
                    authorities: []
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/main/kpi4.html',
                        controller: 'kpi4Controller'
                    }
                },
                resolve: {

                }
            });

    });
