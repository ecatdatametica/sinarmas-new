'use strict';

angular.module('demoAppApp').service('DataSource', function($http,$q,$log){
    this.setpiedata = function(thissetpiedata){
        this.thissetpiedata = thissetpiedata;
    }
    this.getpiedata = function(){
        return this.thissetpiedata;
    }

    this.dashboardPlantation = function(date){
		var deferred=$q.defer();
        $http({method:'GET',url:'/predictive/'+date}).
            success(function (data,status,header,config){
                $log.info(header);
                deferred.resolve(data);

            }).
            error(function (data,status,header,config){
               /* toasty.pop.error({
                    title: 'Error',
                    msg: "Failed to load the database tree",
                    timeout: 1500,
                    showClose: true,
                    clickToClose: true
                });*/
                deferred.reject(status,data);
                $log.info(status);
            });
        return deferred.promise;
    };
    this.dashboardPlantationHistory = function(date){
        var deferred=$q.defer();
        $http({method:'GET',url:'/plantationHistory/'+date}).
            success(function (data,status,header,config){
                $log.info(header);
                deferred.resolve(data);

            }).
            error(function (data,status,header,config){
                /*  toasty.pop.error({
                 title: 'Error',
                 msg: "Failed to load the database tree",
                 timeout: 1500,
                 showClose: true,
                 clickToClose: true
                 });*/
                deferred.reject(status,data);
                $log.info(status);
            });
        return deferred.promise;
    };
    this.dashboardPlantationlist = function(date){
        var deferred=$q.defer();
        $http({method:'GET',url:'/plantation/'+date}).
            success(function (data,status,header,config){
                $log.info(header);
                deferred.resolve(data);

            }).
            error(function (data,status,header,config){
              /*  toasty.pop.error({
                    title: 'Error',
                    msg: "Failed to load the database tree",
                    timeout: 1500,
                    showClose: true,
                    clickToClose: true
                });*/
                deferred.reject(status,data);
                $log.info(status);
            });
        return deferred.promise;
    };
    this.dashboardHarvesting = function(date){
        var deferred=$q.defer();
        $http({method:'GET',url:'/harvesting/'+date}).
            success(function (data,status,header,config){
                $log.info(header);
                deferred.resolve(data);

            }).
            error(function (data,status,header,config){
                /*toasty.pop.error({
                    title: 'Error',
                    msg: "Failed to load the database tree",
                    timeout: 1500,
                    showClose: true,
                    clickToClose: true
                });*/
                deferred.reject(status,data);
                $log.info(status);
            });
        return deferred.promise;
    };
    this.dashboardForEntity = function(date){
        var deferred=$q.defer();
        $http({method:'GET',url:'/predictive/'+date}).
            success(function (data,status,header,config){
                $log.info(header);

                deferred.resolve(data);

            }).
            error(function (data,status,header,config){
               /* toasty.pop.error({
                    title: 'Error',
                    msg: "Failed to load the database tree",
                    timeout: 1500,
                    showClose: true,
                    clickToClose: true
                });*/
                deferred.reject(status,data);
                $log.info(status);
            });
        return deferred.promise;
    };


    this.loadAllDataSources = function(date){
        var deferred=$q.defer();
     
        $http({method:'GET',url:'/snddata/'+date}).
            success(function (data,status,header,config){

               /* toasty.pop.success({
                    title: 'Success',
                    msg: "success",
                    timeout: 1500,
                    showClose: true,
                    clickToClose: true
                });*/
                deferred.resolve(data);

            }).
            error(function (data,status,header,config){
                deferred.reject(status,data);
         /*       toasty.pop.error({
                    title: 'Error',
                    msg: "Failed ",
                    timeout: 1500,
                    showClose: true,
                    clickToClose: true
                });*/
                $log.info(status);
            });
        return deferred.promise;
    };




});


