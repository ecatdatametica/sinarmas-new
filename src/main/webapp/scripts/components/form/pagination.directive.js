/* globals $ */
'use strict';

angular.module('demoAppApp')
    .directive('demoAppAppPagination', function() {
        return {
            templateUrl: 'scripts/components/form/pagination.html'
        };
    });
